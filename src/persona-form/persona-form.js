import { LitElement, html } from "lit-element";

class PersonaForm extends LitElement {
    static get properties() {
        return {
            person: {type: Object}
        };
    }

    constructor() {
        super();

        this.person = {};
    }

    goBack(e) {
        console.log('goBack');	  
        e.preventDefault();	
        this.dispatchEvent(
            new CustomEvent(
                'persona-form-close',
                {}
            )
        );
    }

    updateName(e) {
        console.log('updateName');
        console.log('Actualizando la propiedad name con el valor ' + e.target.value);

        this.person.name = e.target.value;
    }
    updateProfile(e) {
        console.log('updateProfile');
        console.log('Actualizando la propiedad profile con el valor ' + e.target.value);

        this.person.profile = e.target.value;
    }

    updateYearsInCompany(e) {
        console.log('updateYearsInCompany');
        console.log('Actualizando la propiedad yearsInCompany con el valor ' + e.target.value);

        this.person.yearsInCompany = e.target.value;
    }

    storePerson(e) {
        console.log('storePerson');
        e.preventDefault();
        
        this.person.photo = {
            'src': './img/persona.jpg',
            'alt': 'Persona'
        };
            
        console.log('La propiedad name es ' + this.person.name);
        console.log('La propiedad profile es ' + this.person.profile);
        console.log('La propiedad yearsInCompany es ' + this.person.yearsInCompany);	
            
        this.dispatchEvent(
            new CustomEvent(
                'persona-form-store',{
                detail: {
                    person:  {
                            name: this.person.name,
                            profile: this.person.profile,
                            yearsInCompany: this.person.yearsInCompany,
                            photo: this.person.photo
                        }
                    }
                }
            )
        );
    }

    render() {
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input
                            type="text"
                            id="personFormName"
                            class="form-control"
                            placeholder="Nombre Completo"
                            @input="${this.updateName}"
                        />
                        <div class="form-group">
                            <label>Perfil</label>
                            <textarea
                                class="form-control"
                                placeholder="Perfil"
                                rows="5"
                                @input="${this.updateProfile}"
                            >
                            </textarea>
                        <div>
                        <div class="form-group">
                            <label>Años en la empresa</label>
                            <input
                                type="text"
                                class="form-control"
                                placeholder="A&ntilde;os en la empresa"
                                @input="${this.updateYearsInCompany}"
                            />
                        <div>
                        <button
                            class="btn btn-primary"
                            @click="${this.goBack}"
                        >
                            <strong>Atr&aacute;s</strong>
                        </button>
                        <button
                            class="btn btn-success"
                            @click="${this.storePerson}"
                        >
                            <strong>Guardar</strong>
                        </button>
                    </div>
                </form>
            </div>
        `;
    }
}

customElements.define('persona-form', PersonaForm);